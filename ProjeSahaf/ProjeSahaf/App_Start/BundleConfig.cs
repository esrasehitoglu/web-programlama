﻿using System.Web;
using System.Web.Optimization;

namespace ProjeSahaf
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                    "~/Scripts/lib.js",
                     "~/Scripts/easing.js",
                     "~/Scripts/bs.js",
                     "~/Scripts/bxslider.js",
                     "~/Scripts/input-clear.js",
                     "~/Scripts/range-slider.js",
                     "~/Scripts/jquery.zoom.js",
                     "~/Scripts/bookblock.js",
                     "~/Scripts/custom.js",
                     "~/Scripts/social.js",
                     "~/Scripts/jquery.booklet.latest.js",
                     "~/Scripts/_references.js",
                     "~/Scripts/range-slider.js",
                     "~/Scripts/respond.matchmedia.addListener.js",
                     "~/Scripts/ui.js ",
                     "~/Scripts/colortip.js ",
                     "~/Scripts/custom00.js ",
                     "~/Scripts/jquery00.js ",
                     "~/Scripts/jquery01.js ",
                     "~/Scripts/jquery02.js ",
                     "~/Scripts/jquery03.js ",
                     "~/Scripts/jquery-1.js ",
                     "~/Scripts/selectna.js ",
                     //"~/Scripts/shBrushC.js ",
                     //"~/Scripts/shBrushJ.js ",
                     //"~/Scripts/shBrushP.js ",
                     //"~/Scripts/shBrushX.js ",
                     "~/Scripts/sc.min.js ",
                     "~/Scripts/shCore00.js "));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/css/bs.css",
                      "~/Content/css/bxslider.css",
                      "~/Content/css/customIE.css",
                      "~/Content/css/default.css",
                      "~/Content/css/flip-demo.css",
                      "~/Content/css/flip.css",
                      "~/Content/css/font-awesome-ie7.css",
                      "~/Content/css/font-awesome.css",
                      "~/Content/css/jquery.booklet.latest.css",
                      "~/Content/css/main-slider.css",
                       "~/Content/css/noJS.css",
                      "~/Content/css/range-slider.css",
                      "~/Content/css/social.css",
                      "~/Content/css/style.css",
                      "~/Content/css/update-responsive.css",
                      "~/Content/css/shCore00.css",
                      "~/Content/css/shThemeD.css",
                      "~/Content/css/skeleton.css",
                      //"~/Content/css/style000.css",
                      "~/Content/css/base000.css",
                      "~/Content/css/scode.css",
                      "~/Content/css/fonts000.css"
                     ));
        }
    }
}
