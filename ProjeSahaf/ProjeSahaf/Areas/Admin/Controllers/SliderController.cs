﻿using ProjeSahaf.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjeSahaf.Areas.Admin.Controllers
{
    
    public class SliderController : AdminController
    {
        ProjeB121210020EsraSehitoglu1CEntities1 db = new ProjeB121210020EsraSehitoglu1CEntities1();
        // GET: Admin/Slider
        public ActionResult Index()
        {
            var model = db.Slider.OrderByDescending(x => x.SliderID).ToList();
            return View(model);
        }

        public ActionResult Add()
        {
            return View();
        }

        const string imageFolderPath = "/Content/images/slider-images/";
        public ActionResult AddSlider(SliderModel model)
        {

            if (ModelState.IsValid)
            {
                string fileName = string.Empty;
                //Dosya Kaydetme
                if (model.Resim != null && model.Resim.ContentLength > 0)
                {
                    fileName = model.Resim.FileName;
                    var path = Path.Combine(Server.MapPath("~" + imageFolderPath), fileName);
                    model.Resim.SaveAs(path);

                }

                //EF nesnesi oluşturma.
                Slider slider = new Slider();

                slider.BaslangicTarihi = model.BaslangicTarihi;
                slider.BitisTarihi = model.BitisTarihi;
                slider.SliderBaslik = model.SliderBaslik;
                slider.SliderText = model.SliderText;
                slider.YazarAdi = model.YazarAdi;
                slider.Text = model.Text;
                slider.ResimYolu = imageFolderPath + fileName;

                db.Slider.Add(slider);
                db.SaveChanges();
            }

            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = db.Slider.Find(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(Slider model)
        {
            if (ModelState.IsValid) {
                db.Slider.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                return View(model);
            }
            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = db.Slider.Find(id);
            return View(model);
        }

        [HttpGet]
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.Slider.Find(id);
            db.Slider.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}