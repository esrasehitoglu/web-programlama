﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjeSahaf.Areas.Admin.Models
{
    public class SliderModel
    {

        public int SliderID { get; set; }
        public string SliderText { get; set; }
        public Nullable<System.DateTime> BaslangicTarihi { get; set; }
        public Nullable<System.DateTime> BitisTarihi { get; set; }
        public string SliderBaslik { get; set; }
        public string YazarAdi { get; set; }
        public string Text { get; set; }
        public HttpPostedFileBase Resim { get; set; }
    }
}