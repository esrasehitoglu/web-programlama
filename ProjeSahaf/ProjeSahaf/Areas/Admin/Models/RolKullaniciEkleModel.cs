﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace ProjeSahaf.Areas.Admin.Models
{
    public class RolKullaniciEkleModel
    {

        [Display(Name = "Kullanıcı Adı")]
        public string KullaniciAdi { get; set; }
        [Display(Name = "Rol Adı")]
        public string RolAdi { get; set; }
    }
}