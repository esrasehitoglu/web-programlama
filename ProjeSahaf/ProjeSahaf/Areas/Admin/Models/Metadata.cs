﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjeSahaf
{
    public class SliderMetadata
    {
        

        [Display(Name ="Tanıtım Yazısı")]
        public string SliderText { get; set; }


        [Display(Name = "Başlangıç Tarihi")]
        public Nullable<System.DateTime> BaslangicTarihi { get; set; }

        [Display(Name = "Bitiş Tarihi")]
        public Nullable<System.DateTime> BitisTarihi { get; set; }

        [Required]
        [Display(Name = "Kitap Adı")]
        public string SliderBaslik { get; set; }

        [Required]
        [Display(Name = "Yazar Adı")]
        public string YazarAdi { get; set; }

        [Display(Name = "Fiyat")]
        public string Text { get; set; }


    }
}