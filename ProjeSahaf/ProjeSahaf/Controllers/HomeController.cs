﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjeSahaf.Controllers
{
    public class HomeController : Controller
    {
        ProjeB121210020EsraSehitoglu1CEntities1 db = new ProjeB121210020EsraSehitoglu1CEntities1();
        public ActionResult Index()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult _Slider()
        {
            var liste = db.Slider.Where(x => x.BaslangicTarihi < DateTime.Now && x.BitisTarihi > DateTime.Now).OrderByDescending(x => x.SliderID);
            return View(liste);
        }
        [ChildActionOnly]
        public ActionResult _BestSellers()
        {
            var liste = db.Kitaplar.Where(x => x.YayinYili==2016).OrderByDescending(x => x.SatisFiyati);
            return View(liste);
        }
        [ChildActionOnly]
        public ActionResult _TopRated()
        {
            var liste = db.Kitaplar.Where(x => x.Stok>2).OrderByDescending(x => x.KitapAdi);
            return View(liste);
        }

        [ChildActionOnly]
        public ActionResult _Books()
        {
            var liste = db.Kitaplar.OrderByDescending(x => x.KitapAdi);
            return View(liste);
        }

        [ChildActionOnly]
        public ActionResult _Last()
        {
            var liste = db.Kitaplar.OrderByDescending(x => x.EklenmeTarihi);
            return View(liste);
        }

        [ChildActionOnly]
        public ActionResult Author()
        {
            var liste = db.Kitaplar.Where(x => x.YazarID==2).OrderByDescending(x => x.EklenmeTarihi);
            return View(liste);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult OnlineStore()
        {
            ViewBag.Message = "Online page.";

            return View();
        }
        public ActionResult BookReviews()
        {
            

            return View();
        }
    }
}